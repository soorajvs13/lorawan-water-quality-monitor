### LoRaWAN Water Quality Monitor
Monitoring of Dissolved Oxygen level in Water makes use of water detection sensor with unique advantage and LoRaWAN network. The system can monitor water quality automatically, and it is low in cost and does not require people on duty. So the water quality testing is likely to be more economical, convenient and fast. The system has good flexibility. Only by replacing the
corresponding sensors and changing the relevant software programs, this system can be used to monitor other water quality parameters. The operation is simple. The system can be expanded to monitor hydrologic, air pollution, industrial and
agricultural production and so on. It has widespread application and extension value.

### Prerequisites

1. Arduino IDE
2. ULPLoRa (https://gitlab.com/icfoss/OpenIoT/ulplora)
3. Atlas Dissolved Oxygen Sensor Kit
