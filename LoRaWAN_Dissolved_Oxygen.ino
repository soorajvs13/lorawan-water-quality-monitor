/*******************************************************************************
################################################################################
   Project Name : Dissolved Oxygen node with Smart Sleep
################################################################################
   Copyright (c) 2019 Sooraj V S, Ajmi A S,Thomas Telkamp and Matthijs Kooijman

   Permission is hereby granted, free of charge, to anyone
   obtaining a copy of this document and accompanying files,
   to do whatever they want with them without any restriction,
   including, but not limited to, copying, modification and redistribution.
   NO WARRANTY OF ANY KIND IS PROVIDED.

   ToDo:
   - set NWKSKEY
   - set APPKSKEY
   - set DEVADDR
   - optionally comment #define DEBUG
   - optionally comment #define SLEEP
   - change mydata to another (small) static text
   - set SLEEP_INTERVAL in seconds (will be readjusted based on sleep logic)
   - set TX_INTERVAL 
   - set CALIBRATION_INTERVAL 

*******************************************************************************/

#include <Arduino.h>
#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
#include <Wire.h>
#include <LowPower.h>
#include <OneWire.h> 
#include <DallasTemperature.h>
#include <EnableInterrupt.h>


#define address 97               //default I2C ID number for EZO DO Circuit.
#define ONE_WIRE_BUS 8        

#define TRIGGER A1
#define DONEPIN A0

OneWire oneWire(ONE_WIRE_BUS); 
DallasTemperature sensors(&oneWire);

#include <avr/pgmspace.h>
#include <EEPROM.h>

//NODE_1
static const PROGMEM u1_t NWKSKEY[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
static const u1_t PROGMEM APPSKEY[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
static const u4_t DEVADDR = 0x00000000;

// left empty here (we cannot leave them out completely unless
// DISABLE_JOIN is set in config.h, otherwise the linker will complain).
void os_getArtEui (u1_t* buf) { }
void os_getDevEui (u1_t* buf) { }
void os_getDevKey (u1_t* buf) { }

// show debug statements; comment next line to disable debug statements
#define DEBUG

// use low power sleep; comment next line to not use low power sleep
#define SLEEP

static osjob_t sendjob;
bool next_transmission;

int temperature=0;

char computerdata[20];           //we make a 20 byte character array to hold incoming data from a pc/mac/other.
byte received_from_computer = 0; //we need to know how many characters have been received.
byte serial_event = 0;           //a flag to signal when data has been received from the pc/mac/other.
byte code = 0;                   //used to hold the I2C response code.
char do_data[20];                //we make a 20 byte character array to hold incoming data from the D.O. circuit.
byte in_char = 0;                //used as a 1 byte buffer to store inbound bytes from the D.O. Circuit.
byte i = 0;                      //counter used for DO_data array.
int time_ = 575;                 //used to change the delay needed depending on the command sent to the EZO Class DO Circuit.

char *DO;                        //char pointer used in string parsing.
char *sat;                       //char pointer used in string parsing.

float DO_float;                  //float var used to hold the float value of the D.O.
float sat_float;                 //float var used to hold the float value of the percent saturation.

struct {
  short int dissolved_oxygen;
  short int temperature;
  //short int Turbidity;
  short int Volt;
  char error;
} mydata;

bool sendTransmission = 0;
unsigned long previousMillis = 0;        // will store last time LED was updated

// constants won't change:
const long interval = 1000;             // interval at which to blink (milliseconds)

unsigned int SLEEP_INTERVAL = 10;                       //* Multiples of 8 in Seconds


const lmic_pinmap lmic_pins = {
  .nss = 6,
  .rxtx = LMIC_UNUSED_PIN,
  .rst = 5,
  .dio = {2, 3, 4},
};

void onEvent (ev_t ev) {

  switch (ev) {
    case EV_TXCOMPLETE:
    #ifndef SLEEP
      os_setTimedCallback(&sendjob, os_getTime() + sec2osticks(TX_INTERVAL), do_send);
    #else
      next_transmission = true;
    #endif
    break;
  }
}

void do_send(osjob_t* j) {
  

  for (int SenseCount=0; SenseCount<3; SenseCount++){
    ReadSensor(SenseCount);}
    Volt()      ;
    digitalWrite(7,LOW);
  if (LMIC.opmode & OP_TXRXPEND)
    {
      #ifdef DEBUG
      Serial.println(F("OP_TXRXPEND, not sending"));
      #endif
    }
  else
    {
      LMIC_setTxData2(1, (unsigned char *)&mydata, sizeof(mydata) - 1, 0);
      digitalWrite(7,LOW);

      #ifdef DEBUG
      Serial.println(F(" Packet Sent"));
      digitalWrite(7,LOW);
      //next_transmission = true;
      #endif
    }
          

}



float ReadTemperature(){
  float  temp_temperature=0;
  for(int i=0;i<10;i++)
  {
  sensors.requestTemperatures();
  temp_temperature = sensors.getTempCByIndex(0);
  }
   return temp_temperature;
  }


void ReadSensor(int SenseCount)
{ 
      pinMode(7, OUTPUT);
      digitalWrite(7,HIGH);
      float temperature = ReadTemperature();
      
      if(SenseCount==0){                      //temperature compensation
      String temp1  = "t,";
      temp1 += (int) temperature;
      char temperature_string[20] ;
      temp1.toCharArray(temperature_string, 20); //convert integer into string
      #ifdef DEBUG
      Serial.println(temperature_string);
      Serial.println("Temperature Reading Done");
      #endif
      for (int i=0;i<20;i++){
        computerdata[i] = temperature_string[i];
        }
        
      } 
     
      if(SenseCount==1){                      //Read Sensor value
      String temp2  = "r";
      char user_command1 [20];
      temp2.toCharArray(user_command1, 20); //convert integer into string
      #ifdef DEBUG
      Serial.println(user_command1);
      Serial.println("Sensor Reading Done");
       #endif
        for (int i=0;i<20;i++){
        computerdata[i] = user_command1[i];
          }
        } 

      if(SenseCount==2){                      //Read Sensor value
      String temp3  = "sleep";
      char user_command2 [20];
      temp3.toCharArray(user_command2, 20); //convert integer into string
      #ifdef DEBUG
      Serial.println(user_command2);
      Serial.println("Sensor Going to Sleep");
       #endif
        for (int i=0;i<20;i++){
        computerdata[i] = user_command2[i];
          }
        }   

              
                                                     
                                                     //if a command was sent to the EZO device.
    for (i = 0; i <= 20; i++) {                               //set all char to lower case, this is just so this exact sample code can recognize the "sleep" command.
      computerdata[i] = tolower(computerdata[i]);                                 //"Sleep" ≠ "sleep"
    }
    i=0;                                                                          //reset i, we will need it later 
    if (computerdata[0] == 'c' || computerdata[0] == 'r')time_ = 575;             //if a command has been sent to calibrate or take a reading we wait 575ms so that the circuit has time to take the reading.
    else time_ = 250;                                                             //if any other command has been sent we wait only 250ms.
 

    Wire.beginTransmission(address);                                        //call the circuit by its ID number.
    Wire.write(computerdata);                                               //transmit the command that was sent through the serial port.
    Wire.endTransmission();                                                 //end the I2C data transmission.


    if (strcmp(computerdata, "sleep") != 0) {                               //if the command that has been sent is NOT the sleep command, wait the correct amount of time and request data.
                                                                            //if it is the sleep command, we do nothing. Issuing a sleep command and then requesting data will wake the D.O. circuit.

      delay(time_);                                                         //wait the correct amount of time for the circuit to complete its instruction.

      Wire.requestFrom(address, 20, 1);                                     //call the circuit and request 20 bytes (this is more than we need)
      code = Wire.read();                                                   //the first byte is the response code, we read this separately.

      switch (code) {                           //switch case based on what the response code is.
        #ifdef DEBUG
        case 1:                                 //decimal 1.
          Serial.println("Success");            //means the command was successful.
          break;                                //exits the switch case.

        case 2:                                 //decimal 2.
          Serial.println("Failed");             //means the command has failed.
          break;                                //exits the switch case.

        case 254:                               //decimal 254.
          Serial.println("Pending");            //means the command has not yet been finished calculating.
          break;                                //exits the switch case.

        case 255:                               //decimal 255.
          Serial.println("No Data");            //means there is no further data to send.
          break;                                //exits the switch case.
          #endif
      }

      while (Wire.available()) {                 //are there bytes to receive.
        in_char = Wire.read();                   //receive a byte.
        do_data[i] = in_char;                    //load this byte into our array.
        i += 1;                                  //incur the counter for the array element.
        if (in_char == 0) {                      //if we see that we have been sent a null command.
          i = 0;                                 //reset the counter i to 0.
          break;                                 //exit the while loop.
        }
      }
      #ifdef DEBUG
      Serial.println(do_data);                  //print the data.
      Serial.println();                         //this just makes the output easier to read by adding an extra blank line
      #endif 
    }
    

    //if (computerdata[0] == 'r') string_pars(); //uncomment this function if you would like to break up the comma separated string into its individual parts.

      String do_temp_str = do_data;
      int do_temp = do_temp_str.toFloat()*100;
      #ifdef DEBUG
      Serial.println(mydata.dissolved_oxygen);
      #endif

      if(SenseCount==1){
      mydata.dissolved_oxygen = do_temp;
      mydata.temperature = temperature*100;
      }
      
  }






void Volt()
{
  double analogvalue = analogRead(A3);
  double bat_temp = ((analogvalue * 3.3) / 1024); //ADC voltage*Ref. Voltage/1024
  double sum = 0;
  double avg = 0;

  for (byte  i = 0; i < 4; i++)
  {
    sum += bat_temp * 2;
  }
  avg = (sum / 4);
  mydata.Volt = avg * 100;

  #ifdef DEBUG
    Serial.print(F(" Battery Voltage: "));
    Serial.print(avg);
    Serial.println(F(" V"));
    Serial.println(F("#########################################"));
  Serial.flush(); 
  #endif
}





void setup() {
  pinMode(7, OUTPUT);
  digitalWrite(7,HIGH);
  Serial.begin(9600);
  Wire.begin();

    pinMode(DONEPIN, OUTPUT);
    digitalWrite(DONEPIN, LOW);

    pinMode(TRIGGER, INPUT);
    enableInterrupt(TRIGGER, nanoTimerTrigger, FALLING);
  
 // readDoCharacteristicValues();      //read Characteristic Values calibrated from the EEPROM
  #ifdef DEBUG
  Serial.println(" ");
  Serial.println("#########################################");
  Serial.println("......... Water Quality PoC v2.0 ........");
  Serial.println("#########################################");
  delay(1000);
  #endif

  // LMIC init
  os_init();
  // Reset the MAC state. Session and pending data transfers will be discarded.
  LMIC_reset();

  // Set static session parameters. Instead of dynamically establishing a session
  // by joining the network, precomputed session parameters are be provided.
  #ifdef PROGMEM
  // On AVR, these values are stored in flash and only copied to RAM
  // once. Copy them to a temporary buffer here, LMIC_setSession will
  // copy them into a buffer of its own again.
  uint8_t appskey[sizeof(APPSKEY)];
  uint8_t nwkskey[sizeof(NWKSKEY)];
  memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
  memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
  LMIC_setSession (0x1, DEVADDR, nwkskey, appskey);
  #else
  // If not running an AVR with PROGMEM, just use the arrays directly
  LMIC_setSession (0x1, DEVADDR, NWKSKEY, APPSKEY);
  #endif

  #if defined(CFG_eu868)
  // Set up the channels
  //used by the Things Network, which corresponds
  // to the defaults of most gateways. Without this, only three base
  // channels from the LoRaWAN specification are used, which certainly
  // works, so it is good for debugging, but can overload those
  // frequencies, so be sure to configure the full frequency range of
  // your network here (unless your network autoconfigures them).
  // Setting up channels should happen after LMIC_setSession, as that
  // configures the minimal channel set.
  // NA-US channels 0-71 are configured automatically

  LMIC_setupChannel(0, 865062500, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
  LMIC_setupChannel(1, 865402500, DR_RANGE_MAP(DR_SF12, DR_SF7B), BAND_CENTI);      // g-band
  LMIC_setupChannel(2, 865985000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band

  // TTN defines an additional channel at 869.525Mhz using SF9 for class B
  // devices' ping slots. LMIC does not have an easy way to define set this
  // frequency and support for class B is spotty and untested, so this
  // frequency is not configured here.
  #elif defined(CFG_us915)
  // NA-US channels 0-71 are configured automatically
  // but only one group of 8 should (a subband) should be active
  // TTN recommends the second sub band, 1 in a zero based count.
  // https://github.com/TheThingsNetwork/gateway-conf/blob/master/US-global_conf.json
  LMIC_selectSubBand(1);
  #endif

  // Disable link check validation
  LMIC_setLinkCheckMode(0);

  // TTN uses SF9 for its RX2 window.
  // LMIC.dn2Dr = DR_SF10;

  // Set data rate and tx_ready power for uplink (note: txpow seems to be ignored by the library)
  LMIC_setDrTxpow(DR_SF7, 14);
  
  // Start job
   do_send(&sendjob);
}




void loop() {
 
//  int sleepcycles = SLEEP_INTERVAL / 8;  // calculate the number of sleepcycles (8s) given the TX_INTERVAL
//  extern volatile unsigned long timer0_overflow_count;
//  unsigned long currentMillis = millis();
  
//  if (currentMillis - previousMillis >= interval)
//  {
//    previousMillis = currentMillis;
//  }
//  os_runloop_once();

  #ifndef SLEEP
  os_runloop_once();
  #else



  if (next_transmission == false) {
    os_runloop_once();
  }
    
  else {

    #ifdef DEBUG
//    Serial.print(F(" Enter sleeping for "));
//    Serial.print(sleepcycles);
//    Serial.println(F(" cycles of 8 seconds"));
//    Serial.println(F("#########################################"));
//    Serial.flush(); // give the serial print chance to complete

    #endif


//    for (int i = 0; i < sleepcycles; i++)
//    {
      // Enter power down state for 8 s with ADC and BOD module disabled
      LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
//      LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
      //LowPower.idle(SLEEP_8S, ADC_OFF, TIMER2_OFF, TIMER1_OFF, TIMER0_OFF, SPI_OFF, USART0_OFF, TWI_OFF);
      // LMIC uses micros() to keep track of the duty cycle, so
      // hack timer0_overflow for a rude adjustment:
//      cli();
//      timer0_overflow_count += 8 * 64 * clockCyclesPerMicrosecond();
//      sei();
//    }



  #ifdef DEBUG
//    Serial.println(" ");
//    Serial.println("#########################################");
//    Serial.println(F(" Sleep complete"));
//    Serial.println("#########################################");  
//  Serial.flush(); // give the serial print chance to complete
  #endif



    next_transmission = false;

    do_send(&sendjob);
    

  }

#endif
  }

  void nanoTimerTrigger() {
  
    next_transmission = false;  
    os_setCallback (&sendjob, do_send);
    digitalWrite(DONEPIN,HIGH); 
    delay(1);
    digitalWrite(DONEPIN,LOW);
    delay(1); 
        
    }
